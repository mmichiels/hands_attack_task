from utils_psychopy import *
import numpy as np
import itertools
import math
import psychopy
from psychopy import visual
import PIL
import copy

class GameObjs():
    def __init__(self, psy, participant_number):
        self.psy = psy
        self.participant_number = participant_number

        max_points = 100
        min_points = 0
        self.first_o_points = [max_points, min_points, 0]
        self.o_max_points_indices = []
        self.o_min_points_indices = []
        for i, points in enumerate(self.first_o_points):
            if points == max_points:
                self.o_max_points_indices.append(i)
            elif points == min_points:
                self.o_min_points_indices.append(i)

        self.first_o_names = ["Esquivado", "Golpeado"]
        self.optimal_points_info = {"text": "¡Esquivado!", "color": "white"}
        self.suboptimal_points_info = {"text": "¡Te ha dado!", "color": "red"}
        self.normal_text_size = 0.044
        self.small_text_size = 0.03

        self.background_pos = (0, 0)
        self.background_size = (1, 1)
        self.stims_def_pos = [(-0.32, -0.38), (0.32, -0.38), (-0.32, 0.5), (0.32, 0.5)]
        self.stims_def_flip_xy = [(False, False), (True, False), (False, True), (True, True)]
        self.stims_def_size = (0.13, 0.23)
        self.outcomes_def_pos = (0, 0)
        self.outcomes_def_size = (0.16, 0.16)
        verts_hitbox = [[-0.5, 0.32], [-0.145, 0.48], [0.16, 0.42], [0.13, 0.02], [0.38, 0.16],
                        [0.52, 0.16], [0, -0.1]]
        self.poly_hitbox = psychopy.visual.ShapeStim(
            win=self.psy.win,
            units='height',
            fillColor='green',
            vertices=verts_hitbox,  # hand_own_img._verticesBase,
            lineColor='green',
            opacity=0.7)
        # poly_hitbox.pos = hand_own_img.pos
        # poly_hitbox.size = hand_own_img.size

        self.sound_swift = sound.Sound('sounds' + os.sep + 'dodge_1.wav', stereo=False)
        self.sound_slam = sound.Sound('sounds' + os.sep + 'slam_2.wav', stereo=False)
        self.sound_correct = sound.Sound('sounds' + os.sep + 'correct_1.wav', stereo=False)
        self.sound_too_early = sound.Sound('sounds' + os.sep + 'error_1.wav', stereo=False)

        self.load_game_objs()

    def load_game_objs(self):
        self.total_stims = 4
        self.total_backgrounds = 0
        self.total_outcomes = 0
        background_init_str = "background_"
        stim_init_str = "stim_"
        outcome_init_str = "outcome_"
        img_extension = ".png"

        for img_filename in os.listdir(self.psy.dir_game_imgs):
            if img_filename.startswith(background_init_str):
                self.total_backgrounds += 1
                if self.total_backgrounds == 1:
                    img_name, img_extension = os.path.splitext(img_filename)
            elif img_filename.startswith(outcome_init_str):
                self.total_outcomes += 1

        self.backgrounds = self.create_background(img_extension, background_init_str)
        self.stims = self.create_stims(img_extension, stim_init_str)
        self.outcomes = self.create_outcomes(img_extension, outcome_init_str)

        self.fixation_stim = visual.TextStim(win=self.psy.win, name='waiting_fmri',
                                             text='.',
                                             font='Arial',
                                             units='height', pos=(0, 0), height=0.1, wrapWidth=None, ori=0,
                                             color='white', colorSpace='rgb', opacity=1,
                                             languageStyle='LTR',
                                             depth=0.0)
        # self.fixation_stim = visual.Circle(win=self.psy.win, radius=0.1, color="red")
        # name='fixation',
        #                                               text='.',
        #                                               font='Arial',
        #                                               units='height', pos=(0, 0), height=0.4, wrapWidth=None, ori=0,
        #                                               color='red', colorSpace='rgb', opacity=1,
        #                                               languageStyle='LTR',
        #                                               depth=0.0)


        self.waiting_fmri_text = visual.TextStim(win=self.psy.win, name='waiting_fmri',
                                                 text='Esperando resonancia...',
                                                 font='Arial',
                                                 units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                 color='white', colorSpace='rgb', opacity=1,
                                                 languageStyle='LTR',
                                                 depth=0.0)
        self.mid_line = visual.Line(win=self.psy.win,
                                    start=(-0.5, 0),
                                    end=(0.5, 0),
                                    pos=(0, 0),
                                    lineColor="red",
                                    lineWidth=50)

        text_intro_new_block = ""  # Va a empezar un nuevo bloque. \n\n"
        self.instr_block_no_dev_text = '¡Esquiva los golpes! \n\n ' \
                                       'Con el dedo índice de cada mano, mantén presionadas la P y Q para iniciar. Retira el dedo de la tecla para' \
                                       ' retirar la mano que vaya a ser atacada \n\n ' \
                                       'Cuanto más rápido lo hagas,' \
                                       ' ¡más puntos ganarás! (La puntuación máxima es de 10 puntos por esquive). \n\n' \
                                       ' ¡CUIDADO con los amagues! Sabrás que no es un amague ' \
                                       'cuando los dedos de la mano del oponente comiencen a traspasar la línea media. \n'
        self.instr_block_no_dev = visual.TextStim(win=self.psy.win, name='instr_block_no_dev',
                                                  text=text_intro_new_block + self.instr_block_no_dev_text, font='Arial',
                                                  units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                  color='white', colorSpace='rgb', opacity=1,
                                                  languageStyle='LTR',
                                                  depth=0.0)

        self.instr_block_dev_text = '¡¡ATENCIÓN!! ¡Ahora puedes contraatacar con la mano opuesta para obtener más puntos! \n\n' \
                                    'Simplemente levanta el dedo de la mano contraria a la que va dirigido el ataque para contraatacar. \n\n' \
                                    'Puedes seguir esquivando los ataques como hasta ahora pero ganarás más puntos ' \
                                    'si contraatacas. \n\n\n ¡Atención: No habrá amagues en este bloque! \n \n ' \
                                    'NO intentes estrategias raras tipo levantar siempre la misma mano. Te descalificaremos si notamos que intentas hacer trampas o alguna estrategia rara.'
        self.instr_block_dev = visual.TextStim(win=self.psy.win, name='instr_block_dev',
                                               text=text_intro_new_block + self.instr_block_dev_text, font='Arial',
                                               units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                               color='white', colorSpace='rgb', opacity=1,
                                               languageStyle='LTR',
                                               depth=0.0)
                                               
                                               
        self.instr_inter_trial = visual.TextStim(win=self.psy.win, name='instr_block_dev',
                                               text="Mantén presionado con los dedos índice (sin levantarlos) la p y la q para comenzar. \n" \
                                               "Cuando sueltes una de las teclas, la mano se levantará para esquivar el golpe. \n " \
                                               "Este mensaje solo aparecerá en estos primeros trials.", font='Arial',
                                               units='height', pos=(0, -0.35), height=self.small_text_size, wrapWidth=None, ori=0,
                                               color='white', colorSpace='rgb', opacity=1,
                                               languageStyle='LTR',
                                               depth=0.0)
        self.instr_inter_trial_late = visual.TextStim(win=self.psy.win, name='instr_block_dev',
                                                                                      text="Si no comienza el trial, levanta los dedos índice de las teclas y vuelve a mantener la q y la p.", font='Arial',
                                                                                      units='height', pos=(0, -0.35), height=self.small_text_size, wrapWidth=None, ori=0,
                                                                                      color='white', colorSpace='rgb', opacity=1,
                                                                                      languageStyle='LTR',
                                                                                      depth=0.0)
        self.text_points_obj_optimal = visual.TextStim(win=self.psy.win, name='text_points_obj_optimal',
                                                       text=self.optimal_points_info["text"],
                                                       font='Arial',
                                                       units='height', pos=(0, -0.15), height=self.normal_text_size, wrapWidth=None,
                                                       ori=0,
                                                       color=self.optimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                       languageStyle='LTR',
                                                       depth=0.0)

        self.text_points_obj_suboptimal = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                          text=self.suboptimal_points_info["text"],
                                                          font='Arial',
                                                          units='height', pos=(0, -0.15), height=self.small_text_size, wrapWidth=None,
                                                          ori=0,
                                                          color=self.suboptimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                          languageStyle='LTR',
                                                          depth=0.0)

        self.results_block_info = visual.TextStim(win=self.psy.win, name='results_block_info',
                                                  text='',
                                                  font='Arial',
                                                  units='height', pos=(0, 0), height=self.small_text_size, wrapWidth=None, ori=0,
                                                  color='white', colorSpace='rgb', opacity=1,
                                                  languageStyle='LTR',
                                                  depth=0.0)

        self.results_block_end_info = visual.TextStim(win=self.psy.win, name='results_block_end_info',
                                                      text='¡Bloque terminado!',
                                                      font='Arial',
                                                      units='height', pos=(0, 0.2), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)

        self.results_block_dev_info = visual.TextStim(win=self.psy.win, name='results_block_dev_info',
                                                      text='' ,
                                                      font='Arial',
                                                      units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)


    def draw_end_game_info(self, total_points, in_fMRI):
        total_points = round(total_points, 2)
        self.end_game_info = visual.TextStim(win=self.psy.win, name='end_game_info',
                                             text='FIN DEL JUEGO \n\n Puntos totales: {} \n\n '
                                                  'Gracias por participar'.format(total_points),
                                             font='Arial',
                                             units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                             color='white', colorSpace='rgb', opacity=1,
                                             languageStyle='LTR',
                                             depth=0.0)
        if not in_fMRI:
            self.end_game_info.text += " \n\n Por favor, ahora lee el PDF con las instrucciones " \
                                       "para enviar tus resultados."

        self.end_game_info.draw()

    def counterbalancing_s_r_o(self, randomize_outcomes=False):
        stims_indices = list(range(self.total_stims))
        responses_indices = list(range(self.total_responses))
        outcomes_indices = list(range(self.total_outcomes))

        all_s_r_o_combinations = [stims_indices, responses_indices, outcomes_indices]
        all_s_r_o_combinations = list(itertools.product(*all_s_r_o_combinations))

        s_combinations = shift_list_n_times(stims_indices)
        r_combinations = shift_list_n_times(responses_indices)
        o_combinations = shift_list_n_times(outcomes_indices)

        participant_number = self.participant_number
        if participant_number > len(all_s_r_o_combinations) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(all_s_r_o_combinations))) * len(
                all_s_r_o_combinations)

        s_r_o_comb = all_s_r_o_combinations[participant_number]
        s_comb = s_combinations[s_r_o_comb[0]]
        r_comb = r_combinations[s_r_o_comb[1]]

        if randomize_outcomes:
            o_comb = o_combinations[s_r_o_comb[2]]
        else:
            o_comb = list(range(self.total_outcomes))

        return s_comb, r_comb, o_comb

    def create_stims(self, img_extension, init_str):
        stims = []

        for i in range(self.total_stims):
            img = self.load_img(0, img_extension, init_str, self.stims_def_pos[i], self.stims_def_size)
            img.flipHoriz = self.stims_def_flip_xy[i][0]
            img.flipVert = self.stims_def_flip_xy[i][1]

            poly_hitbox = copy.copy(self.poly_hitbox)  #.copy()
            poly_hitbox.flipHoriz = self.stims_def_flip_xy[i][0]
            poly_hitbox.flipVert = self.stims_def_flip_xy[i][1]
            poly_hitbox.size = img.size
            img.hitbox = poly_hitbox
            # img.colorSpace = 'rgb255'
            # img.colorSpace = (0, 128, 255)
            # img_filepath = os.path.join(self.psy.dir_game_imgs, init_str + str(0 + 1) + img_extension)
            # img_pil = PIL.Image.open(img_filepath).convert('L')
            # img_pil.load()
            # img_data = np.asarray(img_pil, dtype="int32")
            # img_mask = img_data > 0
            # img_data[img_mask] = 0
            # img_mask[:, :] = 1
            # img.mask = img_data
            # img.lineColor = (3, 255, 121)
            # img.verticesPix[0, 0] -= 99999
            # img.verticesPix[1, 0] -= 99999
            # img.verticesPix[2, 0] -= 99999
            # img.verticesPix[3, 0] -= 99999
            stim = Stim(self.psy, img)
            stims.append(stim)

        return stims

    def create_outcomes(self, img_extension, init_str):
        outcomes = []

        for i in range(self.total_outcomes):
            points = self.first_o_points[i]
            name = self.first_o_names[i]

            img = self.load_img(i, img_extension, init_str, self.outcomes_def_pos, self.outcomes_def_size)
            outcome = Outcome(self.psy, img, name, points)
            outcomes.append(outcome)

        return outcomes

    def reset_stims_pos(self):
        stims = []

        for i, stim in enumerate(self.stims):
            stim.img.pos = self.stims_def_pos[i]

        return 0


    def create_background(self, img_extension, init_str):
        backgrounds = []

        for i in range(self.total_backgrounds):
            img = self.load_img(0, img_extension, init_str, self.background_pos, self.background_size)
            background = Background(self.psy, img)
            backgrounds.append(background)

        return backgrounds

    def load_img(self, img_idx, img_extension, init_str, pos=None, size=None):
        new_pos = (0, 0) if pos is None else pos
        new_size = (0, 0) if size is None else size

        img_name = os.path.join(self.psy.dir_game_imgs, init_str + str(img_idx + 1) + img_extension)
        img = self.psy.load_img_from_disk(img_name, new_pos, new_size)

        return img

class GameObj():
    def __init__(self, psy, img):
        self.img = img

class Stim(GameObj):
    def __init__(self, psy, img):
        super(Stim, self).__init__(psy, img)

class Background(GameObj):
    def __init__(self, psy, img):
        super(Background, self).__init__(psy, img)

class Outcome(GameObj):
    def __init__(self, psy, img, name, points=0, is_dev=False):
        super(Outcome, self).__init__(psy, img)

        self.name = name
        self.original_points = points
        self.points = points
        self.is_dev = is_dev
        self.create_points_info(psy)

    def create_points_info(self, psy):
        self.points_obj = visual.TextStim(win=psy.win, name='outcome_points',
                                          text='+ {}'.format(self.points),
                                          font='Arial',
                                          units='height', pos=(0, -0.1), height=0.04, wrapWidth=None, ori=0,
                                          color='white', colorSpace='rgb', opacity=1,
                                          languageStyle='LTR',
                                          depth=0.0)

    def devaluate(self, psy, new_points):
        self.points = new_points
        self.is_dev = True
        self.create_points_info(psy)

    def reset_dev(self, psy):
        self.points = self.original_points
        self.is_dev = False
        self.create_points_info(psy)

