import os
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import plotly
import plotly.graph_objs as plotly_graph
import plotly.figure_factory as plotly_figures
# from IPython.display import Image
# Image(img_bytes)
import plotly.io as pio
# pio.renderers.default = "browser"
from plotly.offline import iplot
import scipy.stats as scipy_stats
import statsmodels.api as sm
import statsmodels.stats.anova as sm_anova
import statsmodels.stats.multicomp as sm_multicomp
import statsmodels.formula.api as sm_formulas
import pingouin as pg
from matplotlib.ticker import StrMethodFormatter
os.environ["OUTDATED_IGNORE"] = "1"

# Pandas options
pd.set_option('display.max_columns', None)

# Matplot options
SMALL_SIZE = 12
MEDIUM_SIZE = 30
BIGGER_SIZE = 24

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
# matplotlib.rc('figure', figsize=(12, 8))

# TODO: Bayes factor
# Guide of all interaction and post-hoc tests: https://github.com/maximtrp/scikit-posthocs

class Analysis():
    def __init__(self, experiment_number=1, psy=None, indep_vars=["num_block", "feint_type"], dep_vars=[]):
        if psy is None:
            self.get_filepaths()
        else:
            self.dir_results = psy.dir_results
            self.results_file_prefix = psy.results_file_prefix
            self.participant_info_file_prefix = psy.participant_info_file_prefix

        self.experiment_number = experiment_number
        self.dir_results_stats = os.path.join('.', 'results_stats')
        self.subj_names = []
        if not os.path.isdir(self.dir_results_stats):
            os.mkdir(self.dir_results_stats)

        self.indep_vars = indep_vars
        self.dep_vars = dep_vars
        self.map_aggregate_trials = {
            "num_trial": "count",
            "dodge_accuracy": proportion(),
            "avoid_feint_accuracy": proportion(),
            "counterattack_accuracy": proportion(),
            "counterattack_try": proportion(),
            "counterattack_try_accuracy": proportion(),
            "counterattack_response_time": "mean",
            "response_time": "mean",
        }
        self.results_participants = self.load_data()

        self.num_participants = len(self.results_participants)
        # self.drop_invalid_participants(max_consumption_trials_errors_per_block)
        # if drop_consumption_trials:
        #     self.drop_invalid_trials()  # Like free consumption trials.

        self.separate_training_dev_blocks(agg_all_by="num_trial")  # self.results_participants_training, self.results_participants_dev, self.results_agg_dev =
        # self.construct_rs()  # outcome_dev_selected variable
        # self.construct_r_switch()  # response_switch variable
        # self.construct_rt_switch(save_csv_participants=True)  # response_time_switch_cost variable
        try:
            self.results_agg_dev_soft_expanded = self.expand_participants_data(self.results_agg_dev_soft)
            self.results_agg_dev_expanded = self.expand_participants_data(self.results_agg_dev)
            self.results_agg_training_expanded = self.expand_participants_data(self.results_agg_training)
        except Exception as e:
            print("WARNING: ", e)
            print("This is probably due to a subject/s not having all the conditions when "
                  "separating the data into training and dev")

        self.results_agg_all_expanded = self.expand_participants_data(self.results_agg_all)

    def drop_invalid_participants(self, max_consumption_trials_errors_per_block):
        # TODO: filter participants by bad performance in free consumptions trials in dev blocks:
        # 1st: get only (filter) free consumptions trials for every participant in all dev blocks (or try in separate too).
        # 2nd: aggregate results to obtain percentage of response_accuracy
        # 3rd: find those participants with response_accuracy lower than X% (90% ?) and drop them.

        # results_participants_dev = self.filter_data_by_col_vals(self.results_participants, "outcome_dev_id", [-1],
        #                                                         inverse=True)
        # results_participants_consumption = self.filter_data_by_col_vals(results_participants_dev,
        #                                                                 "trial_type", [1])
        # num_consumption_trials = results_participants_consumption[0].shape[0]
        # results_participants_consumption, results_agg_consumption = self.group_data(results_participants_consumption,
        #                                                                             ["num_block"])
        # num_consumption_trials_per_block = int(num_consumption_trials / results_participants_consumption[0].shape[0])
        # threshold_accuracy = (num_consumption_trials_per_block - max_consumption_trials_errors_per_block) / num_consumption_trials_per_block
        #
        # participants_remove_indices = []
        # # participants_remove_indices = [32]
        # for i, partic in enumerate(results_participants_consumption):
        #     if partic.loc[0, "response_accuracy"] < threshold_accuracy or \
        #             partic.loc[1, "response_accuracy"] < threshold_accuracy:
        #         participants_remove_indices.append(i)
        #
        # self.results_participants = [partic for i, partic in enumerate(self.results_participants)
        #                              if i not in participants_remove_indices]
        # self.num_participants = len(self.results_participants)
        #
        # print("Removed {} participants that had more than {} errors in consumption trials in dev blocks".format(
        #     len(participants_remove_indices), max_consumption_trials_errors_per_block))
        # print("--------Total participants for analysis: {}---------------------------".format(
        #     self.num_participants))

        self.num_participants = len(self.results_participants)

        return 0

    def drop_invalid_trials(self):
        self.results_participants = self.filter_data_by_col_vals(self.results_participants,
                                                                 "trial_type", [1], inverse=True)

    def custom_filter_data(self, participants_dfs, filter_name):
        if filter_name == "trials_dev":
            for i, results_participant in enumerate(participants_dfs):
                for index, row in results_participant.iterrows():
                    outcome_dev_id = row["outcome_dev_id"]
                    results_participant.loc[index, "trial_dev"] = -1
                    if outcome_dev_id != -1:
                        results_participant.loc[index, "trial_dev"] = 0
                        if outcome_dev_id == row["outcome_selected_id"] or \
                                outcome_dev_id == row["outcome_not_selected_id"]:  # Trial where we can obtain the devalued outcome
                            results_participant.loc[index, "trial_dev"] = 1
        else:
            raise Exception("Custom filter: {} does not exist.".format(filter_name))

        participants_data = self.filter_data_by_col_vals(participants_dfs,
                                                                 "trial_dev", [0], inverse=True)
        return participants_data

    def separate_training_dev_blocks(self, agg_all_by="num_trial", num_block_for_baseline=1):
        self.results_participants_training = self.filter_data_by_col_vals(self.results_participants, "curr_block_type",
                                                                          ["training"])
        self.results_participants_dev_soft = self.filter_data_by_col_vals(self.results_participants, "curr_block_type",
                                                                     ["dev_soft"])
        self.results_participants_dev = self.filter_data_by_col_vals(self.results_participants, "curr_block_type",
                                                                     ["dev"])
        # Preceding training block/s for baseline dev data:
        # preceding_num_block = self.results_participants_dev[0].loc[:, "num_block"].iloc[0] - 1
        self.results_participants_training_last_block = self.filter_data_by_col_vals(self.results_participants,
                                                                                     "num_block", [num_block_for_baseline])

        results_participants_agg_dev_soft, self.results_agg_dev_soft = self.group_data(self.results_participants_dev_soft,
                                                                             self.indep_vars)

        results_participants_agg_dev, self.results_agg_dev = self.group_data(self.results_participants_dev,
                                                                             self.indep_vars)

        results_participants_training, self.results_agg_training = self.group_data(self.results_participants_training,
                                                                                   self.indep_vars)

        results_participants_agg_all, self.results_agg_all = self.group_data(self.results_participants, agg_all_by,
                                                                             agg_by_participants=False)

    def construct_rs(self):
        # RS = Response selections = outcome_dev_selected: proportion of responses in a devaluation block leading to
        # the outcome which was devalued, that is, proportion of responses consistent with use of a habit

        # Proportion of RS of the now outcome_dev_id in the preceding training block
        outcome_dev_id = self.results_participants_dev[0].loc[:, "outcome_dev_id"].iloc[0]
        self.map_aggregate_trials["outcome_selected_id"] = proportion_by_val(outcome_dev_id)
        results_participant_by_block_baseline, results_by_block_baseline = self.group_data(
            self.results_participants_training_last_block, self.indep_vars)

        # RS - baseline:
        rs_baseline_data = np.array(results_by_block_baseline.loc[:, "outcome_selected_id"].values[0])
        self.results_agg_dev["outcome_dev_selected_baselined"] = self.results_agg_dev.loc[:,
                                                                 "outcome_dev_selected"] \
            .apply(lambda x: np.array(x) - rs_baseline_data)

        outcome_dev = self.results_agg_dev.loc[:, "outcome_dev_selected"]
        outcome_dev_selected_baselined = self.results_agg_dev.loc[:, "outcome_dev_selected_baselined"]

        return outcome_dev, outcome_dev_selected_baselined

    def construct_r_switch(self):
        # RT switch cost = response time swich cost = response_switch: Trials where participants correctly
        # changed their response to avoid a devalued outcome.

        # Proportion of RT switch answers (trials with same stim_id and response_id) in the preceding training block
        results_participants_rt_switch = self.filter_data_by_col_vals(self.results_participants_dev,
                                                                      "response_switch", [1])
        stim_ids = []
        response_ids = []
        for participant in results_participants_rt_switch:
            stim_ids.append(list(participant.loc[:, "stim_id"].values))
            response_ids.append(list(participant.loc[:, "response_id"].values))

        results_participants_rt_switch_baseline = self.filter_data_by_col_vals(self.results_participants_training_last_block,
                                                                               "stim_id", vals_per_participant=stim_ids)
        results_participants_rt_switch_baseline = self.filter_data_by_col_vals(results_participants_rt_switch_baseline,
                                                                               "response_id",
                                                                               vals_per_participant=response_ids)

        count_vals_agg = [participant_df.shape[0] for participant_df in results_participants_rt_switch_baseline]
        results_participant_by_block_baseline, results_by_block_baseline = self.group_data(
            self.results_participants_training_last_block, self.indep_vars, count_vals_agg=count_vals_agg)

        rt_switch_baseline_data = np.array(results_by_block_baseline.loc[:, "response_switch"].values[0])
        self.results_agg_dev["response_switch_baselined"] = self.results_agg_dev.loc[:, "response_switch"] \
            .apply(lambda x: np.array(x) - rt_switch_baseline_data)

        response_switch = self.results_agg_dev.loc[:, "response_switch"]
        response_switch_baselined = self.results_agg_dev.loc[:, "response_switch_baselined"]

        return response_switch, response_switch_baselined

    def construct_rt_switch(self, baseline=True, save_csv_participants=True):
        # RT switch cost = response time swich cost: RT in trials where participants correctly
        # changed their response to avoid a devalued outcome.

        # Baselined as in Luque et al 2020: RTs for trials with the same stimulus (Shigh/Slow) when participants
        # made the ‘standard’ response in the preceding No-Dev block (e.g., R1 for trials with S1 high)
        results_participants_rt_switch_cost_trials = self.results_participants_dev
        results_participants_rt_switch_cost, results_agg_rt_switch_cost = self.group_data(results_participants_rt_switch_cost_trials,
                                                                                          self.indep_vars)
        results_participants_rt_switch_baseline = self.filter_data_by_col_vals(self.results_participants_training_last_block,
                                                                               "response_accuracy",
                                                                               vals=[1])

        blocks = set(results_agg_rt_switch_cost.index.get_level_values("num_block"))
        stim_types = set(results_agg_rt_switch_cost.index.get_level_values("is_overtraining_stim"))
        for subj_id, subj_df in enumerate(results_participants_rt_switch_cost_trials):
            subj_df_baseline = results_participants_rt_switch_baseline[subj_id]

            for blck in blocks:
                filter_indices = subj_df["num_block"] == blck
                results_by_block_dev = subj_df[filter_indices]

                for stim_type in stim_types:
                    subj_df = results_participants_rt_switch_cost_trials[subj_id]
                    stim_type = int(stim_type)
                    filter_indices = results_by_block_dev["is_overtraining_stim"] == stim_type
                    results_by_block_dev_this = results_by_block_dev[filter_indices]
                    filter_indices = results_by_block_dev_this["response_switch"] == 1
                    results_by_block_dev_this = results_by_block_dev_this[filter_indices]

                    filter_indices = subj_df_baseline["is_overtraining_stim"] == stim_type
                    results_by_block_baseline_this = subj_df_baseline[filter_indices]
                    rt_baselined = np.mean(results_by_block_baseline_this["response_time"])

                    rt_switch_cost = results_by_block_dev_this["response_time"]
                    rt_switch_cost_baselined = results_by_block_dev_this["response_time"] - \
                                                                            rt_baselined
                    trial_ids = rt_switch_cost_baselined.index
                    subj_new_df = results_participants_rt_switch_cost_trials[subj_id].loc[trial_ids]
                    subj_new_df["rt_switch_cost"] = rt_switch_cost
                    subj_new_df["rt_switch_cost_baselined"] = rt_switch_cost_baselined
                    results_participants_rt_switch_cost_trials[subj_id] = results_participants_rt_switch_cost_trials[subj_id].combine_first(subj_new_df)

        results_participants_rt_switch_cost_trials_, results_agg = self.group_data(results_participants_rt_switch_cost_trials,
                                                                                                   self.indep_vars, remove_nan=False)
        if self.results_agg_dev is None:
            self.results_agg_dev = results_agg
        else:
            self.results_agg_dev.update(results_agg)

        rt_switch_cost = self.results_agg_dev.loc[:, "rt_switch_cost"]
        rt_switch_cost_baselined = self.results_agg_dev.loc[:, "rt_switch_cost_baselined"]

        results_modified_dir = os.path.join(self.dir_results, "results_v1_fmri_with_rt_switch_cost")
        if not os.path.exists(results_modified_dir):
            os.mkdir(results_modified_dir)

        if save_csv_participants:
            print("---- NOTE -----")
            print("Make sure you have drop_consumption_trials=False in Analysis() IF you want to reconstruct the whole csv")
            for subj_id, subj_dev_df in enumerate(results_participants_rt_switch_cost_trials):
                filename = subj_dev_df["filename"].iloc[0]
                df_path = os.path.join(results_modified_dir, filename)
                subj_training_df = self.results_participants_training[subj_id]
                # TODO: Concatenate consumption trials too
                subj_df = pd.concat([subj_training_df, subj_dev_df])
                subj_df = subj_df.sort_values("num_trial")
                subj_df.to_csv(df_path)

        return rt_switch_cost, rt_switch_cost_baselined

    def expand_participants_data(self, df):
        subj_names = self.subj_names * len(df.index)
        subj_indices = list(range(self.num_participants)) * len(df.index)
        df.reset_index(inplace=True)
        df = df.apply(pd.Series.explode)
        df = df.reset_index(inplace=False)
        df["subj_idx"] = subj_indices
        df["subj_name"] = subj_names

        return df

    def anova(self, data, dep_var, indep_vars, nans_correction="mean"):
        data[dep_var] = data[dep_var].astype('float')
        data[indep_vars] = data[indep_vars].astype('category')
        data = data.loc[:, [dep_var]+indep_vars+["subj_idx"]]

        print("----ANOVA Repeated measures (nans_correction: {}) ----".format(nans_correction))

        if nans_correction == "remove":
            nans_indices = np.where(np.isnan(data[dep_var]))[0]
            nans_subjs = list(data.loc[nans_indices, "subj_idx"])
            nans_subjs = list(set(nans_subjs))  # To remove repetitions
            remove_indices = ~(data["subj_idx"].isin(nans_subjs))
            data = data.loc[remove_indices, :]

            anova_table = sm_anova.AnovaRM(data, depvar=dep_var, subject='subj_idx', within=indep_vars)
            anova_table = anova_table.fit()
            print(anova_table)
        elif nans_correction == "mean":
            # data = pg.remove_rm_na(data=data, dv=dep_var, within=indep_vars, subject="subj_idx", aggregate='mean')
            anova_table = pg.rm_anova(dv=dep_var, within=indep_vars, subject='subj_idx', data=data, detailed=True)
            if len(indep_vars) == 1:
                print(anova_table.loc[:, ["SS", "MS", "F" , "p-unc", "np2"]])  # "sphericity"
            else:
                print(anova_table.loc[:, ["Source", "F", "p-unc", "p-GG-corr"]])  # "sphericity"
            #  This is the "same" as running:
            # data.loc[:, dep_var].fillna(data.loc[:, dep_var].mean(), inplace=True)
            # anova_table = sm_anova.AnovaRM(data, depvar=dep_var, subject='subj_idx', within=indep_vars)
            # res = anova_table.fit()
            # print(res)
        else:
            raise Exception("No nans correction method called {}".format(nans_correction))

        # ANOVA (non-repeated measures):
        # ols_formula_str = '{}  ~ '.format(dep_var)
        # ols_interactions_str = ''
        # for i, indep_var in enumerate(indep_vars):
        #     ols_formula_str += 'C({}) + '.format(indep_var)
        #     if i < len(indep_vars)-1:
        #         ols_interactions_str += 'C({}):'.format(indep_var)
        #     else:
        #         ols_interactions_str += 'C({})'.format(indep_var)
        # ols_formula_str += ols_interactions_str
        #
        # model_ols = sm_formulas.ols(ols_formula_str, data=data).fit()
        # anova_table = sm.stats.anova_lm(model_ols, typ=2)
        # # print(model_ols.summary())
        # print("ANOVA (non-repeated measures) table:")
        # print(anova_table)

        return anova_table, data

    def post_hoc(self, data, dep_var, indep_vars, correction="Holm", nans_correction="welch_t_test"):
        # Post-hoc (post-anova in this case) t-tests to check the effect of specific pairs of groups
        results = []
        results_str = ""

        for indep_var in indep_vars:
            if nans_correction == "welch_t_test":
                # Pingouin It will automatically uses Welch T-test when the sample sizes are
                # unequal, as recommended by Zimmerman 2004.
                pass
            elif nans_correction == "remove":
                nans_indices = np.where(np.isnan(data[dep_var]))[0]
                nans_subjs = list(data.loc[nans_indices, "subj_idx"])
                nans_subjs = list(set(nans_subjs))  # To remove repetitions
                remove_indices = ~(data["subj_idx"].isin(nans_subjs))
                data = data.loc[remove_indices, :]
            elif nans_correction == "mean":
                data.loc[:, dep_var].fillna(data.loc[:, dep_var].mean(), inplace=True)
            else:
                raise Exception("No nans correction method called {}".format(nans_correction))

            multicomp_results = pg.pairwise_ttests(data=data, dv=dep_var, within=indep_var, subject='subj_idx',
                                                   padjust=correction)
            results.append(multicomp_results)
            results_str += str(multicomp_results)
            print("Post-hoc results (nans_correction: {}) for {} - {}:".format(nans_correction, dep_var, indep_var))
            print(multicomp_results)

        return results, results_str

    def get_filepaths(self):
        this_dir = os.path.dirname(os.path.abspath(__file__))
        project_dir = os.path.join(this_dir, '..')
        os.chdir(project_dir)

        self.dir_results = os.path.join('.', 'results', 'v1_psinvestiga_hands_attack')
        self.results_file_prefix = ""  # "results"
        self.participant_info_file_prefix = "info_"

    def load_data(self):
        results_each_participant = []
        filenames_sorted = os.listdir(self.dir_results)
        # filepaths_sorted = [os.path.join(self.dir_results, filename) for filename in filenames]
        # filepaths_by_date.sort(key=lambda x: os.path.getmtime(x))
        filenames_sorted.sort()

        for i, filename in enumerate(filenames_sorted):
            filepath = os.path.join(self.dir_results, filename)
            is_csv_results_file = filename.startswith(self.results_file_prefix) and filename.endswith('.csv')
            if is_csv_results_file and "_tmp" not in filename:
                results_participant = pd.read_csv(filepath, sep=',', index_col=0)

                mask_training_blocks = results_participant["num_block"].isin([0, 1, 2])
                results_participant.loc[mask_training_blocks, "curr_block_type"] = "training"
                mask_dev_soft_blocks = results_participant["num_block"].isin([3, 4, 5])
                results_participant.loc[mask_dev_soft_blocks, "curr_block_type"] = "dev_soft"
                mask_dev_blocks = results_participant["num_block"].isin([7, 8, 9])
                results_participant.loc[mask_dev_blocks, "curr_block_type"] = "dev"

                mask_overtraining = results_participant["opponent_hand_direction"] == "parallel"
                results_participant.loc[mask_overtraining, "is_overtraining_stim"] = 1
                results_participant.loc[~mask_overtraining, "is_overtraining_stim"] = 0

                mask_feint = results_participant["feint_type"] == 0

                mask_dodge_accuracy = results_participant["dodge_ok"] == 1
                mask_avoid_feint_accuracy = mask_feint & mask_dodge_accuracy
                results_participant.loc[:, "avoid_feint_accuracy"] = np.nan
                results_participant.loc[mask_feint, "avoid_feint_accuracy"] = 0
                results_participant.loc[mask_avoid_feint_accuracy, "avoid_feint_accuracy"] = 1

                results_participant.loc[:, "dodge_accuracy"] = results_participant["dodge_ok"]
                results_participant.loc[mask_feint, "dodge_accuracy"] = np.nan
                try:
                    self.dep_vars.index("dodge_accuracy")
                    results_participant.loc[mask_feint, "response_time"] = np.nan
                except ValueError as e:
                    pass

                results_participant.loc[:, "counterattack_accuracy"] = results_participant["counterattack_ok"]
                results_participant.loc[:, "counterattack_try_accuracy"] = results_participant["counterattack_ok"]
                mask_counterattack_not_tried = results_participant["counterattack_ok"] == -1
                results_participant.loc[mask_counterattack_not_tried, "counterattack_try_accuracy"] = np.nan
                results_participant.loc[mask_counterattack_not_tried, "counterattack_accuracy"] = np.nan
                mask_counterattack_not_tried_in_dev = mask_counterattack_not_tried & mask_dev_blocks
                results_participant.loc[~mask_dev_blocks, "counterattack_accuracy"] = np.nan
                results_participant.loc[mask_counterattack_not_tried_in_dev, "counterattack_accuracy"] = 0

                results_participant.loc[~mask_counterattack_not_tried, "counterattack_try"] = 1
                results_participant.loc[mask_counterattack_not_tried_in_dev, "counterattack_try"] = 0
                # results_participant.loc[~mask_counterattack_not_tried, "dodge_accuracy"] = 0
                results_participant.loc[:, "counterattack_response_time"] = results_participant["response_time"]
                results_participant.loc[mask_counterattack_not_tried, "counterattack_response_time"] = np.nan

                results_participant["filename"] = [filename] * results_participant.shape[0]
                results_each_participant.append(results_participant)
                print("File {} of {} loaded: {}".format(i, len(filenames_sorted), filename))
                print("Num rows: {}".format(results_participant.shape[0]))
                subj_name = filename.split(".csv")[0]
                self.subj_names.append(subj_name)

        print("Loading complete. {} files loaded".format(len(results_each_participant)))

        return results_each_participant

    def group_data(self, results_each_participant, cols_agg=["num_block", "is_overtraining_stim"], count_vals_agg=None,
                   agg_by_participants=True, remove_nan=False):
        results_participants = []

        for i, results_part in enumerate(results_each_participant):
            results_part_agg = results_part
            if agg_by_participants:
                results_part_agg = self.pd_group_and_aggr(results_part, cols_agg, remove_nan)
            results_participants.append(results_part_agg)

        participants_idx = range(len(results_participants))
        results_agg_pd = pd.concat(results_participants, axis=0, keys=participants_idx)
        results_agg_pd = results_agg_pd.groupby(cols_agg).agg(list)

        return results_participants, results_agg_pd

    def pd_group_and_aggr(self, df, cols, remove_nan=False):
        if remove_nan:
            df = df.dropna(axis=0)
        df = df.groupby(cols).agg(self.map_aggregate_trials).reset_index()

        return df

    def filter_data_by_col_vals(self, participants_dfs, col, vals=None, vals_per_participant=None, inverse=False):
        results_participants_filtered = []
        for i, results_participant in enumerate(participants_dfs):
            vals_filter = vals if vals_per_participant is None else vals_per_participant[i]
            mask = results_participant[col].isin(vals_filter)
            if inverse:
                mask = ~mask
            results_participant_filtered = results_participant.loc[mask, :]
            results_participants_filtered.append(results_participant_filtered)

        return results_participants_filtered

    def plot_two_scatter(self, df, x_names, remove_extreme_vals=[1, 1], group_by="is_overtraining_stim",
                         agg_by="num_trial", type_blocks="training", save_file=True, num_stds_show=1):
        if type_blocks != "all":
            mask_type_blocks = df["curr_block_type"] == type_blocks
            df = df[mask_type_blocks]

        groups = list(np.unique(df.loc[:, group_by])) + [None]
        if "reload_rt" in x_names:
            mask = df["reload_rt"].isna()
            df = df[~mask]
        for group in groups:
            if group is None:
                group = "all"
                df_group = df
            else:
                mask = df[group_by] == group
                df_group = df[mask]
            df_group = df_group.groupby(agg_by).agg(list)

            # is_stim_overtraining = df_group.iloc[0]["is_overtraining_stim"][0]
            # is_stim_dev = df_group.iloc[0]["outcome_dev_id"][0] == group

            y = df_group.loc[:, x_names]

            if isinstance(y.iloc[0, 0], list):
                if remove_extreme_vals is not None:
                    for i, var in enumerate(x_names):
                        y.loc[:, var] = y.loc[:, var].apply(lambda y:
                                                            [row for row in y if i <= remove_extreme_vals[i]])

                y = y.applymap(lambda y: [i for i in y if i <= 1])
                y_mean = y.applymap(lambda y: np.mean(y))
                y_std = y.applymap(lambda y: np.std(y))

            is_continuous = is_series_cont(y_mean.loc[:, x_names[0]])

            if is_continuous:
                x = list(y.index)
                mean_var_1 = y_mean.loc[:, x_names[0]]
                std_var_1 = y_std.loc[:, x_names[0]]
                mean_var_2 = y_mean.loc[:, x_names[1]]
                std_var_2 = y_std.loc[:, x_names[1]]
                fig, ax1 = plt.subplots()

                color = 'tab:blue'
                ax1.set_xlabel(agg_by)
                ax1.set_ylabel(x_names[0], color=color)

                # plt.errorbar(x, mean_var_1, yerr=std_var_1, fmt='o', markersize=8, lw=3, capsize=20, color=color)
                ax1.plot(x, mean_var_1, color=color)
                plt.fill_between(x, mean_var_1 - num_stds_show*std_var_1, mean_var_1 + num_stds_show*std_var_1, alpha=0.2, color=color)
                ax1.tick_params(axis='y', labelcolor=color)

                ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

                color = 'tab:green'
                ax2.set_ylabel(x_names[1], color=color)  # we already handled the x-label with ax1
                # plt.errorbar(x, mean_var_2, yerr=std_var_2, fmt='o', markersize=8, lw=3, capsize=20, color=color)
                ax2.plot(x, mean_var_2, color=color)
                plt.fill_between(x, mean_var_2 - num_stds_show*std_var_2, mean_var_2 + num_stds_show*std_var_2, alpha=0.2, color=color)
                ax2.tick_params(axis='y', labelcolor=color)

                # plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}')) # No decimal places
                plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.2}'))  # 2 decimal places
                ax1.set_xticks(x)
                # fig.tight_layout()  # otherwise the right y-label is slightly clipped
                # fig.set_title('Title', pad=20)
                plt.title("{}_{}__{}_blocks_({}*std)".format(group_by, group, type_blocks ,num_stds_show))

                filepath = os.path.join(self.dir_results_stats, "scatter_all_trials_{}_{}_({}_{}__{}_blocks_{}_stds).svg".format(x_names,
                                                                                                                                 agg_by, group_by, group, type_blocks,
                                                                                                                                 num_stds_show))
                if save_file:
                    plt.savefig(filepath, format="svg")

            plt.show()

    def boxplot_x_by_group(self, df, x_names, aggr_by=["num_block", "is_overtraining_stim"],
                           overtrained_labels=False, save_file=True):
        boxprops = dict(linestyle='-', linewidth=4, color='blue')
        medianprops = dict(linestyle='-', linewidth=4, color='red')
        capprops = dict(linestyle='-', linewidth=4, color='blue')
        whiskerprops = dict(linestyle='-', linewidth=4, color='blue')

        boxplot = df.boxplot(column=x_names, by=aggr_by, boxprops=boxprops, medianprops=medianprops, showfliers=False,
                             capprops=capprops, whiskerprops=whiskerprops)
        if "accuracy" in x_names[0]:
            plt.ylim(ymin=0)  # TODO: Uncomment this to visually compare accuracy conditions

        if overtrained_labels:
            xlabels = ["Little trained", "Overtrained"]
            ax = plt.gca()
            ax.set_xticklabels(xlabels)
            # ax.set_title('')
            ax.set_xlabel("");
            plt.suptitle('')

        filepath = os.path.join(self.dir_results_stats, "boxplot_{}_{}.svg".format(x_names, aggr_by))
        if save_file:
            plt.savefig(filepath, format="svg")

        plt.show()

    def plot_means_ci_x_by_group(self, df, x_names, aggr_by=["num_block", "is_overtraining_stim"],
                                 overtrained_labels=False, save_file=True):
        data_grouped = df.groupby(aggr_by)[x_names]
        mean = data_grouped.mean().T.squeeze()
        std = data_grouped.std().T.squeeze()

        if overtrained_labels:
            xvals = ["Little trained", "Overtrained"]
        else:
            xvals = mean.index

        # plt.scatter(xvals, mean)
        uplims = None
        if "accuracy" in x_names[0]:
            plt.ylim(ymax=1)
            ymin = np.min(mean) - 1.05*np.max(std)
            plt.ylim(ymin=ymin)

        plotline1, caplines1, barlinecols1 = plt.errorbar(xvals, mean, yerr=std, fmt='o', markersize=15, lw=3,
                                                          capsize=7, capthick=3)
        ax = plt.gca()
        ax.set_xmargin(0.8)
        plt.title('means_ci_95_y_{}_x_{}'.format(x_names, aggr_by))
        filepath = os.path.join(self.dir_results_stats, "means_ci_95_{}_{}.svg".format(x_names, aggr_by))
        if save_file:
            plt.savefig(filepath, format="svg")

        plt.show()

    def save_output_csv(self, participants_data, aggr_results_csv_by=None, custom_filter=None):
        if custom_filter is not None:
            participants_data = self.custom_filter_data(participants_data, custom_filter)

        # participants_data = self.filter_data_by_col_vals(participants_data,"is_overtraining_stim", [1],
        #                                                  inverse=True)  # TODO: comment this line
        results_participants_agg, results_agg = self.group_data(participants_data, self.indep_vars)

        try:
            df = self.expand_participants_data(results_agg)

            if aggr_results_csv_by is not None:
                # df = self.pd_group_and_aggr(df, aggr_results_csv_by, remove_nan=True)
                df = df.groupby(aggr_results_csv_by).agg(proportion()).reset_index()

            df.loc[:, "response_not_switch"] = 1 - df.loc[:, "response_switch"]
            output_file = os.path.join(self.dir_results_stats, "output_df.csv")
            df.to_csv(output_file, index=False)
        except Exception as e:
            print("ERROR saving file: ", e)
            print("This is probably due to a subject/s not having all the conditions when "
                  "separating the data into training and dev. (e.g. one subject doesn't have reload trials when "
                  "the stim is overtrained in the 7th block")

    def save_results_str(self, input_str):
        output_file = os.path.join(self.dir_results_stats, "results_stats.txt")
        with open(output_file, "w") as text_file:
            text_file.write(input_str)

    def filter_participants_by_response_accuracy(self, num_block="all", threshold=80):
        participants_remove_inds = []
        total_participants = len(self.results_participants)

        for i, partic in enumerate(self.results_participants):
            if num_block != "all":
                partic = partic[partic["num_block"] == num_block]
            num_correct_trials = (partic["response_accuracy"].values == 1).sum()
            percentage_correct = num_correct_trials / partic.shape[0] * 100
            print("Participant {} - Percentage correct trials: {} %".format(i, percentage_correct))
            if percentage_correct < threshold:
                participants_remove_inds.append(i)
                print("Participant {} removed".format(i))

        num_partic_removed = len(participants_remove_inds)
        percentage_partic_removed = num_partic_removed / len((self.results_participants)) * 100
        print("Num participants removed: {} ({} % of {} total participants)".format(num_partic_removed, percentage_partic_removed, total_participants))
        self.results_participants.remove(participants_remove_inds)

def proportion():
    return lambda x: x.sum() / x.count()

def proportion_by_val(val=0, vals_sum=None):
    return lambda x: ((x==val).sum() if vals_sum is None else vals_sum) / x.count()

def save_fig_to_file(figure, dir_name, file_name):
    filepath_html = os.path.join(dir_name, file_name) + ".html"
    filepath_png = os.path.join(dir_name, file_name) + ".png"

    # plotly.offline.plot(figure, filename=filepath_html)
    figure.write_image(filepath_png)

def scatter_univariate(x, column_x_name="Trials"):
    y = list(range(x.shape[0]))

    trace_scatter = plotly_graph.Scattergl(
        x=y,
        y=x,
        name="Scatter",
        text=x.name,
        xaxis='x',
        yaxis='y',
        mode='lines',
        marker=dict(
            size=10,
            line=dict(
                width=0.8,
                color='rgb(0, 0, 0, .9)',
            )
        ),
    )

    layout = set_layout(title="Scatter univariate", column_x_name=column_x_name,
                        column_y_name=x.name, all_x_labels=0)
    figure = plotly_graph.Figure(data=[trace_scatter], layout=layout)


    return figure

def is_series_cont(x):
    continuous = True

    if x.dtype in ['object', 'bool']:
        continuous = False

    return continuous

def set_layout(title="", column_x_name="", column_y_name="", all_x_labels=1, height=600, show_y_zero_line=True):
    layout = plotly_graph.Layout(
        title=title,
        autosize=True,
        height=height,
        margin=plotly_graph.layout.Margin(
        ),
        xaxis=dict(
            title=column_x_name,
            automargin=True,
            dtick=all_x_labels,
            zeroline=show_y_zero_line,
        ),
        yaxis=dict(
            title=column_y_name,
            automargin=True,
        ),
    )

    return layout



if __name__ == "__main__":
    indep_vars = ["num_block", "is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
    dep_vars = ["num_block", "is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
    plot_all_agg_vars = ["response_time", "counterattack_try_accuracy"]  # avoid_feint_accuracy counterattack_response_time counterattack_accuracy counterattack_try_accuracy counterattack_trt
    # plot_all_agg_vars = ["response_time", "avoid_feint_accuracy"]  # avoid_feint_accuracy
    aggr_results_csv_by = ["subj_name"]  # Default: None; is_overtraining_stim is for when you correlate the BOLD
    custom_filter = "trials_dev"  # This select the X specified rows by the filter. Default: None; trials_dev selects the trials in the dev blocks where we can obtain the devalued outcome
    experiment_number = 1
    type_blocks = "dev"  # all training dev_soft dev
    agg_by_plots = "num_block"  # num_block num_trial
    num_stds_show = 0.05
    anonymize = False
    if type_blocks == "all":
        # indep_vars = ["num_block", "is_overtraining_stim"]
        indep_vars = ["num_block", "feint_type"]
        # dep_vars = ['dodge_accuracy', 'response_time']
        # dep_vars = ['avoid_feint_accuracy', 'response_time']
    elif type_blocks == "training":
        indep_vars = ["num_block", "is_overtraining_stim"]
        # dep_vars = ['dodge_accuracy', 'response_time']
        dep_vars = ['avoid_feint_accuracy', 'response_time']
    elif type_blocks == "dev_soft":
        # indep_vars = ["dev_soft_change_side_trial", "is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
        indep_vars = ["num_block", "is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
        # dep_vars = ['dodge_accuracy', 'response_time']
        dep_vars = ['avoid_feint_accuracy', 'response_time']
    elif type_blocks == "dev":
        indep_vars = ["num_block", "is_overtraining_stim"]
        dep_vars = ['counterattack_accuracy', 'counterattack_response_time']

    analysis = Analysis(experiment_number, indep_vars=indep_vars, dep_vars=dep_vars)

    # analysis.filter_participants_by_response_accuracy(num_block=6, threshold=82)

    analysis.plot_two_scatter(analysis.results_agg_all_expanded, plot_all_agg_vars, remove_extreme_vals=[1, 1],
                              agg_by=agg_by_plots, num_stds_show=num_stds_show, type_blocks="all", group_by="is_overtraining_stim")
    # analysis.plot_two_scatter(analysis.results_agg_all_expanded, plot_all_agg_vars, remove_extreme_vals=[1, 1],
    #                           agg_by=agg_by_plots, num_stds_show=num_stds_show, type_blocks="all", group_by="outcome_dev_id")
    # It doesn't make sense to plot_two_scatter group by stim_id because the dev stim for dev block 1 is different
    # between participants (it's counterbalanced).
    participants_data = None
    if type_blocks == "all":
        indep_vars = ["is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
        plot_indep_vars = ["is_overtraining_stim"]
        data = analysis.results_agg_all_expanded
        participants_data = analysis.results_participants
    elif type_blocks == "training":
        plot_indep_vars = ["is_overtraining_stim"]
        data = analysis.results_agg_training_expanded
        participants_data = analysis.results_participants_training
    elif type_blocks == "dev_soft":
        # indep_vars = ["is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
        plot_indep_vars = ["is_overtraining_stim"]
        data = analysis.results_agg_dev_soft_expanded
        participants_data = analysis.results_participants_dev_soft
    elif type_blocks == "dev":
        indep_vars = ["is_overtraining_stim"]  # ["num_block", "is_overtraining_stim"]
        plot_indep_vars = indep_vars
        data = analysis.results_agg_dev_expanded
        participants_data = analysis.results_participants_dev

    analysis.plot_two_scatter(analysis.results_agg_all_expanded, plot_all_agg_vars, remove_extreme_vals=[1, 1],
                              agg_by=agg_by_plots, num_stds_show=num_stds_show, type_blocks=type_blocks)
    if anonymize:
        data["subj_name"].drop(inplace=True)

    stats_results_str = ""
    for dep_var in dep_vars:
        print("---------------{} stats------------".format(dep_var))
        anova_table_rs = analysis.anova(data, dep_var, indep_vars)
        post_hoc_results, post_hoc_results_str = analysis.post_hoc(data, dep_var, indep_vars)

        stats_results_str += str(anova_table_rs)
        stats_results_str += post_hoc_results_str
        is_overtrained_cond = False
        if plot_indep_vars == ["is_overtraining_stim"]:
            is_overtrained_cond = True
        analysis.boxplot_x_by_group(data, [dep_var], aggr_by=plot_indep_vars,
                                    overtrained_labels=is_overtrained_cond)
        analysis.plot_means_ci_x_by_group(data, [dep_var], aggr_by=plot_indep_vars,
        overtrained_labels=is_overtrained_cond)

    analysis.save_results_str(stats_results_str)
    # analysis.save_output_csv(participants_data, aggr_results_csv_by, custom_filter)  # This file can be used for fMRI correlations
