import time

from utils_psychopy import *
import conn_and_wait_trigger_fmri as conn_fmri
import numpy as np
import multiprocessing
import itertools
import math
import random
import game_objs
import events
import psychopy

import psychtoolbox as ptb

class Experiment:
    def __init__(self, psy):
        self.psy = psy
        self.session_number = int(psy.exp_info["session_number"])
        self.participant_number = int(psy.exp_info["participant_number"])
        print("PARTICIPANT NUMBER: ", self.participant_number)
        self.in_fMRI = psy.exp_info['in_fMRI']
        self.objs = game_objs.GameObjs(self.psy, self.participant_number)  # Psychopy objects (stimuli, responses, etc)
        self.clock_total_time = psychopy.core.Clock()
        self.clock_onsets = psychopy.core.Clock()
        self.kb = psychopy.hardware.keyboard.Keyboard()

        # Block-trials config (every block has num_trials_per_block; Total trials = num_blocks*num_trials_per_block)

        # Controls
        if self.in_fMRI:
            self.instruction_keys = {
                "backward_key": "num_1",
                "forward_key": "num_2",
                "exit_key": "escape"
            }
            self.left_decision_key = "num_1"
            self.right_decision_key = "num_2"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            self.next_screen_key = "space"
            self.press_space_text = ""
        else:
            self.instruction_keys = {
                "backward_key": "left",
                "forward_key": "right",
                "exit_key": "escape"
            }
            self.left_decision_key = "q"
            self.right_decision_key = "p"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            self.next_screen_key = "space"
            self.press_space_text = "Presiona espacio para continuar."

        if self.session_number == 0:
            self.num_trials_per_block = 20  # At least 4
            self.training_per_over_stim = 0.5  # There are one types of overtraining stimuli and one types of undertraining stimuli
            self.training_times_over_time = 3.5  # 3.5
            self.midline_y = 0
            self.feint_type_1_percentage = 0.30
            self.feint_type_1_percentage_degradation = 0.7
            self.feint_type_1_percentage_counterattacks = 0.0  # TODO: 0.0
            self.feint_type_2_percentage = 0.0
            self.feints_distances_y = [0.12, 0.2]
            self.blocks_design = [
                # "training",
                "dev",
#                "contingency_degradation",
            ]

            self.feints_speed_parallel = 0.05  # TODO: Set it at 0.05
            self.feints_speed_diagonal = 0.054  # TODO: Set it at 0.05
            self.opponent_hands_speed_parallel = 0.066  # TODO: Set it at 0.066 (i.e. 0.52 s)
            self.opponent_hands_speed_diagonal = 0.077  # TODO: Set it at 0.077 (i.e. 0.49 s)
            self.own_hands_speed = 0.0277  # TODO: Set it at 0.0277
            self.own_hands_counterattack_speed = 0.4  # TODO: Set it at 0.4

            # Duration screens:20 
            self.duration_fixation = 0.25
            self.duration_fixation_minimum = 0.25
            self.time_till_hint_keys = 2
            self.duration_instr_no_dev = 2.0
            self.duration_till_attack = 0.3
            self.duration_between_response_outcome = 0.2
            self.duration_outcome_presentation = 1.1  # 1.4
            self.duration_iti_before_trial = 0.1  # ISI (inter-stimulus interval)
            self.duration_end_game_screen = 10
            self.duration_max_jitter = 0
        elif self.session_number <= 2:
            self.num_trials_per_block = 36  # 36 # At least 4
            self.training_per_over_stim = 0.5  # There are one types of overtraining stimuli and one types of undertraining stimuli
            self.training_times_over_time = 3.5  # 3.5
            self.midline_y = 0
            self.feint_type_1_percentage = 0.3  # TODO: Set it at 0.30
            self.feint_type_1_percentage_degradation = 0.7  # TODO: Set it at 0.7
            self.feint_type_1_percentage_counterattacks = 0.0
            self.feint_type_2_percentage = 0.0
            self.feints_distances_y = [0.12, 0.2]
            self.blocks_design = ["training"] * 3 # 3
            self.blocks_design += ["contingency_degradation"] * 3  # 3
            self.blocks_design += ["training"] * 1  # 1
            self.blocks_design += ["dev"] * 3  # 3

            self.feints_speed_parallel = 0.05  # TODO: Set it at 0.05
            self.feints_speed_diagonal = 0.05  # TODO: Set it at 0.05
            self.opponent_hands_speed_parallel = 0.066  # TODO: Set it at 0.066
            self.opponent_hands_speed_diagonal = 0.0726  # TODO: Set it at 0.0726
            self.own_hands_speed = 0.0277  # TODO: Set it at 0.0277
            self.own_hands_counterattack_speed = 0.4  # TODO: Set it at 0.4

            # Duration screens:20
            self.duration_fixation = 0.25
            self.duration_fixation_minimum = 0.25
            self.time_till_hint_keys = 2
            self.duration_instr_no_dev = 2.0
            self.duration_till_attack = 0.3
            self.duration_between_response_outcome = 0.2
            self.duration_outcome_presentation = 1.1  # 1.4
            self.duration_iti_before_trial = 0.1  # ISI (inter-stimulus interval)
            self.duration_end_game_screen = 10
            self.duration_max_jitter = 0
        elif self.session_number == 3:
            pass

        self.num_blocks = len(self.blocks_design)
        self.max_consecutive_stims = 3
        self.num_overtraining_stim, self.num_undertraining_stim = self.overtraining_stims_stats(self.training_per_over_stim)
        self.stim_overtraining_indices = list(range(self.num_overtraining_stim))
        self.stim_undertraining_indices = list(range(self.num_overtraining_stim, self.num_undertraining_stim))
        self.blocks_config_indices = self.calculate_blocks_config_indices()

        # Stim imgs config
        self.stim_imgs_hashmap = {
            "left__own": 0,
            "right__own": 1,
            "left__opponent": 2,
            "right__opponent": 3,
        }

        self.conds_hashmap = {  # Design table: Stimulus id -> Response id -> outcome id
            0: {  # Condition 0 (overtrained)
                "left__opponent": "left__own",  # Left hand moves in parallel (his left vs my left)
            },
            1: {  # Condition 1 (overtrained)
                "right__opponent": "right__own",  # Right hand moves in parallel
            },
            2: {  # Condition 2 (undertrained)
                "left__opponent": "right__own",  # Left hand moves in diagonal
            },
            3: {  # Condition 3 (undertrained)
                "right__opponent": "left__own",  # Right hand moves in diagonal
            },
            # 4: {  # Condition 4 (undertrained) - Both hands moves in parallel
            #     "left__opponent": "left__own",  # Left hand moves in parallel
            #    "right__opponent": "right__own",  # Right hand moves in parallel
            #},
        }

        self.responses_hashmap = {
            0: "left__own",
            1: "right__own",
        }

        self.outcome_codes_hashmap = {
            "no_error": "¡Esquivado!",
            "no_error_feint": "¡Bien! ¡No caiste en el amague!",
            "no_error_feint_both": "¡Bien! ¡No caiste en el amague! \n ¡No levantes ambas manos!",
            "no_error_both": "¡Esquivado! \n ¡Pero no levantes ambas manos!",
            "too_early": "¡Demasiado pronto!",
            "too_early_both": "¡Demasiado pronto! \n ¡No levantes ambas manos!",
            "too_late": "¡Te ha dado!",
            "too_late_both": "¡Te ha dado! \n ¡No levantes ambas manos!",
            "opposite_hand": "¡Moviste la mano opuesta!",
            "opposite_hand_both": "¡Moviste la mano opuesta! \n ¡No levantes ambas manos!",
            "counterattack_ok": "¡Buen contraataque!",
            "counterattack_error": "¡Contraataque fallido!",
        }

        self.outcomes_accuracy_imgs_hashmap = {
            False: 0,
            True: 1,
        }

        # Participant results
        self.results = {
            "num_trial": [],
            "num_block": [],
            "opponent_hand_idx": [],
            "opponent_hand_direction": [],
            "own_hand_moving_idx": [],
            "own_hand_target_idx": [],
            "own_hand_moving_other_idx": [],
            "dodge_ok": [],
            "feint_type": [],
            "counterattack_ok": [],
            "response_time": [],
            # "own_hand_x": [],
            # "own_hand_y": [],
            "opponent_hand_x": [],
            "opponent_hand_y": [],
            "points_this_trial": [],
            "points_this_block": [],
            "outcome_code": [],
            "onset_iti": [],
            "onset_fixation": [],
            "onset_stimulus": [],
            "onset_attack_starts": [],
            "onset_attack_ends": [],
            "onset_dodge_starts": [],
            "onset_dodge_ends": [],
            "onset_counterattack_starts": [],
            "onset_counterattack_ends": [],
            "onset_outcome_presentation": [],
            "onset_end_trial": [],
            "total_points": [],
            "total_time_end_trial": [],
            "last_trial_dropped_frames": [],
        }

    def run(self):
        for i, dev_type in enumerate(self.blocks_config_indices):
            self.dev_type = dev_type
            self.block(i)

        self.objs.draw_end_game_info(self.results["total_points"][-1], in_fMRI=self.in_fMRI)
        self.psy.win.flip()
        psychopy.core.wait(self.duration_end_game_screen)

        return 0

    def block(self, num_block):
        self.curr_feint_type_1_percentage = self.feint_type_1_percentage

        if self.dev_type == -1 or self.dev_type == 1:  # No devaluation
            training_times_over_time = self.training_times_over_time

            if self.dev_type == 1:
                self.curr_feint_type_1_percentage = self.feint_type_1_percentage_degradation
                training_times_over_time = 1

            self.conds_indices, self.feints_indices = self.calculate_conds_indices(self.num_trials_per_block,
                                                              self.training_per_over_stim,
                                                              training_times_over_time,
                                                              self.dev_type)
            conds_indices = self.conds_indices
            self.current_outcome_dev = None
            if num_block == 0:
                self.objs.instr_block_no_dev.text = self.objs.instr_block_no_dev_text + '\n\n {}'.format(self.press_space_text)
                self.objs.instr_block_no_dev.draw()
                self.psy.win.flip()
            psychopy.core.wait(self.duration_instr_no_dev)
        elif self.dev_type == 0:
            training_times_over_time = 1
            self.curr_feint_type_1_percentage = self.feint_type_1_percentage_counterattacks
            self.conds_indices, self.feints_indices = self.calculate_conds_indices(self.num_trials_per_block,
                                                              self.training_per_over_stim,
                                                              training_times_over_time,
                                                              self.dev_type)
            conds_indices = self.conds_indices
            self.current_outcome_dev = None
            self.objs.instr_block_dev.text = self.objs.instr_block_dev_text + '\n\n {}'.format(self.press_space_text)
            self.objs.instr_block_dev.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_instr_no_dev)

        points_this_block = 0
        events.get_response_wait([self.next_screen_key])

        if self.in_fMRI:
            events.get_response_wait([self.next_screen_key])  # Second confirmation key press
            self.objs.waiting_fmri_text.draw()
            self.psy.win.flip()
            new_clock_total_time = conn_fmri.conn_and_wait_for_MRI_trigger_dsr()
            if num_block == 0:
                self.clock_total_time = new_clock_total_time
                print("TIME reset: ", self.clock_total_time.getTime())

        all_trials_dropped_frames = 0
        self.psy.win.recordFrameIntervals = True
        for j, cond_idx in enumerate(conds_indices):  # Each trial must contain at least one trial of each stimuli?
            self.psy.win.flip(clearBuffer=True)

            # psychopy.logging.console.setLevel(psychopy.logging.WARNING)
            feint_type = self.feints_indices[j]
            if len(self.results["num_trial"]) == 0:
                last_num_trial = 0
            else:
                last_num_trial = self.results["num_trial"][-1]
            num_trial = last_num_trial+1

            self.trial(cond_idx, feint_type, num_trial)
            last_trial_dropped_frames = self.psy.win.nDroppedFrames - all_trials_dropped_frames
            all_trials_dropped_frames = self.psy.win.nDroppedFrames
            print('------ In the trial {}, {} frames were dropped.'.format(j, last_trial_dropped_frames))
            print('------ In total, {} frames were dropped.'.format(all_trials_dropped_frames))
            print('====')

            self.results["num_block"].append(num_block)
            self.results["num_trial"].append(num_trial)
            points_this_block += self.results["points_this_trial"][-1]
            self.results["points_this_block"].append(points_this_block)
            self.results["total_time_end_trial"].append(self.clock_total_time.getTime())
            self.results["last_trial_dropped_frames"].append(last_trial_dropped_frames)
            results_df = self.psy.save_results(self.results, format="csv", tmp=True)

        # Show temp results to the user

        results_this_block_mask = results_df.loc[:, "num_block"] == num_block
        results_this_block = results_df[results_this_block_mask]
        num_trials_this_block = results_this_block.shape[0]
        dodges_this_block_mask = results_this_block.loc[:, "dodge_ok"] == True
        dodges_this_block = results_this_block[dodges_this_block_mask]
        num_dodges_this_block = dodges_this_block.shape[0]
        mean_dodge_speed_this_block = np.round(dodges_this_block.loc[:, "response_time"].mean(), 2)
        if np.isnan(mean_dodge_speed_this_block):
            mean_dodge_speed_this_block = 0
        points_this_block = round(self.results["points_this_block"][-1], 3)
        points_total = round(self.results["total_points"][-1], 3)

        self.objs.results_block_info.text = "¡Bloque {} de {} terminado!" \
                                            " \n\n Puntos en este bloque: {} " \
                                            "\n Puntos totales: {}" \
                                            "\n Esquives en este bloque: {} de {}" \
                                            "\n Velocidad media de los esquives en este bloque: {} segundos" \
                                            "\n ¡Bien hecho pero intenta ser más rápido!" \
                                            " \n\n {}". \
            format(num_block+1, self.num_blocks, points_this_block,
                   points_total, num_dodges_this_block, num_trials_this_block,
                   mean_dodge_speed_this_block, self.press_space_text)
        self.objs.results_block_info.draw()
        self.psy.win.flip()

        # if self.dev_type != -1:
        #     self.objs.results_block_dev_info.text = '\n ' \
        #                                             '\n\n {}'.format(self.press_space_text)
        #     self.objs.results_block_end_info.draw()
        #     self.objs.results_block_dev_info.draw()
        #     # self.current_outcome_dev.img.draw()
        #     self.psy.win.flip()

        self.psy.save_results(self.results, format="csv", tmp=True)
        events.get_response_wait([self.next_screen_key])
        # logging.flush()

    def trial(self, cond_idx, feint_type, num_trial, show_countdown_text=False):
        onsets = {}

        if num_trial == 1:
            self.clock_onsets.reset()

        onsets["iti"] = self.wait_iti(num_trial, show_countdown_text)

        # Fixation
        # self.objs.fixation_stim.draw()
        self.objs.fixation_stim.autoDraw = True
        onsets["fixation"] = self.clock_onsets.getTime()
        if num_trial <= 5:
            # self.objs.instr_inter_trial.draw()
            self.objs.instr_inter_trial.autoDraw = True
        self.psy.win.flip()
        psychopy.core.wait(self.duration_fixation_minimum)

        self.kb.clock.reset()  # when you want to start the timer from

        time_start_check = time.time()
        while True:
            self.psy.win.flip()
            curr_time = time.time()
            time_till_now = curr_time - time_start_check
            if num_trial > 5 and time_till_now >= self.time_till_hint_keys:
                self.objs.instr_inter_trial_late.autoDraw = True

            keys_pressed = self.kb.getKeys(waitRelease=False, keyList=["p", "q"], clear=False)
            if len(keys_pressed) > 0:
                # print(keys_pressed[0].name, keys_pressed[0].duration)
                if keys_pressed[0].duration is not None:
                    self.kb.clearEvents()
            if len(keys_pressed) >= 2 and keys_pressed[0].duration is None and keys_pressed[1].duration is None:
                break
            else:
                pass
                # print("Not enough keys: ", len(keys_pressed))

        self.objs.fixation_stim.autoDraw = False
        self.objs.instr_inter_trial_late.autoDraw = False
        if num_trial <= 5:
            self.objs.instr_inter_trial.autoDraw = False

        # psychopy.core.wait(self.duration_fixation)
        psychopy.event.clearEvents()

        # Background img
        self.objs.backgrounds[0].img.autoDraw = True
        # self.objs.mid_line.autoDraw = True

        # Stimulus img
        self.objs.reset_stims_pos()
        for stim in self.objs.stims:
            stim.img.pos *= 1  # Hack to update the state of the flip image
            stim.img.autoDraw = True

        onsets["stimulus"] = self.clock_onsets.getTime()
        self.psy.win.flip()

        # Take response
        allowed_keys = self.control_keys
        clock_response = psychopy.core.Clock()  # Start timing response
        time_consumed = 0
        response_time = None
        response_idx = -1
        time_to_wait_response = 2
        time_limit = False
        delay_attack_response = 10
        attack_time = np.random.uniform(self.duration_till_attack, time_to_wait_response)
        dodge_started = False
        counterattack_started = False
        dodge_ends = False
        counterattack_ends = False
        attack_started = False
        attack_ends = False
        hand_own_img = None
        hand_name_own = None
        hand_name_own_moving = None
        hand_own_moving_img = None
        hand_name_opponent = None
        # prob_feint = np.random.random()
        # feint_type = -1
        # # prob_feint = 0  # TODO: Delete this line
        # if prob_feint > 0.7:
        #     feint_type = np.random.randint(low=0, high=len(self.feints_distances_y))

        time_attack_starts = None
        time_attack_ends = None
        move_towards = None
        onsets["attack_starts"] = None
        onsets["attack_ends"] = None
        onsets["dodge_starts"] = None
        onsets["dodge_ends"] = None
        onsets["counterattack_starts"] = None
        onsets["counterattack_ends"] = None
        opponent_hand_pos = None
        hand_name_own_moving_other = None
        mouse = psychopy.event.Mouse(visible=False)

        while True:
            keys_pressed = self.kb.getKeys(waitRelease=True)
            # print(keys_pressed[0].name)
            time_consumed = clock_response.getTime()
            if time_consumed >= attack_time and not attack_ends:
                if not attack_started:
                    hand_name_own, hand_name_opponent, move_towards_attack = self.attack_hand(cond_idx)
                    attack_started = True
                    time_attack_starts = time.time()
                    onsets["attack_starts"] = self.clock_onsets.getTime()
                    # print("Attack started")
                else:
                    hand_opponent_img, attack_ends = self.move_hand(hand_name_opponent, move_towards_attack, feint_type)

                    hand_opponent_img.hitbox.pos = hand_opponent_img.pos
                    hand_opponent_y = hand_opponent_img.pos[1] - hand_opponent_img.size[1] / 2
                    hand_own_idx = self.stim_imgs_hashmap[hand_name_own]
                    hand_own_img = self.objs.stims[hand_own_idx].img
                    if hand_own_img is not None:
                        hand_own_img.hitbox.pos = hand_own_img.pos
                        if hand_opponent_y < self.midline_y:
                            collision = hand_own_img.hitbox.overlaps(hand_opponent_img.hitbox)
                            if collision:
                                break
                    if counterattack_started and hand_own_moving_img is not None:
                        hand_own_moving_img.hitbox.pos = hand_own_moving_img.pos
                        if hand_opponent_y < self.midline_y:
                            collision = hand_own_moving_img.hitbox.overlaps(hand_opponent_img.hitbox)
                            if collision:
                                break

            if attack_ends and feint_type != -1:
                # print("-------------reset")
                hand_opponent_img, feint_ends = self.move_hand(hand_name_opponent, reset_pos=True)
                if feint_ends:
                    opponent_hand_pos = self.get_hand_pos(hand_name_opponent)
                    break

            # attack_ends = time_consumed >= (attack_time + delay_attack_response)
            if (attack_ends and feint_type == -1) or (time_limit and time_consumed > time_to_wait_response):
                break

            if dodge_started:
                hand_own_img, dodge_ends = self.move_hand(hand_name_own_moving)  # Dodge attack

            if counterattack_started:
                hand_own_moving_img, counterattack_ends = self.move_hand(hand_name_own_moving, move_towards=move_towards_counterattack)

            if dodge_ends:
                onsets["dodge_ends"] = self.clock_onsets.getTime()
                break

            if counterattack_ends:
                onsets["counterattack_ends"] = self.clock_onsets.getTime()
                break

            if len(keys_pressed) > 0:
                pass
            # if len(keys_pressed) == 1:
                this_key = keys_pressed[0]
                try:
                    response_idx = allowed_keys.index(this_key)
                    # response_time = time_consumed
                    if not dodge_started and not counterattack_started:
                        # print("---Dodge started")
                        dodge_started = True
                        self.objs.sound_swift.play()
                        hand_name_own_moving = self.responses_hashmap[response_idx]
                        opponent_hand_pos = self.get_hand_pos(hand_name_opponent)

                        if self.dev_type == 0:
                            if hand_name_opponent is not None:
                                response_time = time.time() - time_attack_starts
                                if hand_name_own != hand_name_own_moving:
                                    onsets["counterattack_starts"] = self.clock_onsets.getTime()
                                    counterattack_started = True
                                    dodge_started = False
                                    _, _, move_towards_counterattack = self.counterattack_hand(cond_idx)
                        else:
                            if time_attack_starts is not None:
                                response_time = time.time() - time_attack_starts
                            onsets["dodge_starts"] = self.clock_onsets.getTime()
                    if dodge_ends:
                        break
                    if counterattack_ends:
                        break
                except ValueError:
                    pass

        print("RESPONSE TIME: ", response_time)
        time_attack_ends = time.time()
        if time_attack_starts is not None:
            time_attack_duration = time_attack_ends - time_attack_starts
        else:
            time_attack_duration = None
        onsets["attack_ends"] = self.clock_onsets.getTime()
        print("Attack duration: ", time_attack_duration)
        if opponent_hand_pos is None:
            opponent_hand_pos = self.get_hand_pos(hand_name_opponent)

        hand_name_own_moving_other = None
        other_key_pressed = self.kb.getKeys(waitRelease=False, keyList=["p", "q"], clear=False)
        if len(other_key_pressed) == 0:
            hand_name_own_moving_other = True
        else:
            hand_name_own_moving_other = False

            # this_key_other = other_key_pressed[0]
            # response_idx_other = allowed_keys.index(this_key_other)
            # hand_name_own_moving_other = self.responses_hashmap[response_idx_other]

        # --- Clear screen ---
        # psychopy.core.wait(self.duration_between_response_outcome)
        # self.kb.clearEvents()
        # keys_pressed = self.kb.getKeys(waitRelease=False, keyList=["p", "q"], clear=True)
        psychopy.event.clearEvents()
        for stim in self.objs.stims:
            stim.img.pos *= 1  # Hack to update the state of the flip image
            stim.img.autoDraw = False
        # self.objs.mid_line.autoDraw = False
        self.objs.backgrounds[0].img.autoDraw = False
        self.psy.win.flip()

        # --- Outcome presentation ---
        response_correct, outcome_code = self.check_correct(hand_name_own, hand_name_own_moving, hand_name_own_moving_other,
                                                          hand_name_opponent, feint_type)

        if not response_correct:
            points = 0
        else:
            if response_time is not None:
                if outcome_code == "counterattack_ok":
                    points = 100 - (response_time*4)  #
                else:
                    points = 10 - (response_time*4)
            else:
                points = 10  # Dodge ok

        counterattack_ok = -1
        if outcome_code == "counterattack_ok":
            counterattack_ok = 1
        elif outcome_code == "counterattack_error":
            counterattack_ok = 0

        opponent_hand_direction = None
        if hand_name_opponent is not None:
            hand_opponent_side = hand_name_opponent.split("__")[0]
            hand_own_side = hand_name_own.split("__")[0]
            if hand_opponent_side == hand_own_side:
                opponent_hand_direction = "parallel"
            else:
                opponent_hand_direction = "diagonal"

        self.draw_outcome_chosen(response_correct, points, outcome_code, self.duration_between_response_outcome)
        self.psy.win.flip()

        onsets["outcome_presentation"] = self.clock_onsets.getTime()
        psychopy.core.wait(self.duration_outcome_presentation)
        self.psy.win.flip()

        onsets["end_trial"] = self.clock_onsets.getTime()

        if opponent_hand_pos is None:
            opponent_hand_pos = (np.nan, np.nan)

        # Update results
        results_dict = {
            "opponent_hand_idx": hand_name_opponent,
            "opponent_hand_direction": opponent_hand_direction,
            "own_hand_moving_idx": hand_name_own_moving,
            "own_hand_target_idx": hand_name_own,
            "own_hand_moving_other_idx": hand_name_own_moving_other,
            "dodge_ok": int(response_correct),
            "feint_type": feint_type,
            "counterattack_ok": counterattack_ok,
            "response_time": response_time,
            # "own_hand_x": own_hand_pos[0],
            # "own_hand_y": own_hand_pos[1],
            "opponent_hand_x": opponent_hand_pos[0],
            "opponent_hand_y": opponent_hand_pos[1],
            "points_this_trial": points,
            "outcome_code": outcome_code,
        }
        self.update_results(results_dict, onsets)

    def attack_hand(self, stim_idx):
        cond = self.conds_hashmap[stim_idx]
        hand_name_own = list(cond.values())[0]
        hand_name_opponent = list(cond.keys())[0]
        hand_side_own = hand_name_own.split("__")[0]
        hand_side_opponent = hand_name_opponent.split("__")[0]

        move_towards = (0, 0)
        if hand_side_opponent == hand_side_own:
            move_towards = (0, -0.3)
        elif hand_side_opponent == "left" and hand_side_own == "right":
            move_towards = (0.25, -0.3)  # (0.82
        elif hand_side_opponent == "right" and hand_side_own == "left":
            move_towards = (-0.25, -0.3)  # (0.82

        return hand_name_own, hand_name_opponent, move_towards

    def counterattack_hand(self, stim_idx):
        cond = self.conds_hashmap[stim_idx]
        hand_name_own = list(cond.values())[0]
        hand_name_opponent = list(cond.keys())[0]
        hand_side_own = hand_name_own.split("__")[0]
        hand_side_opponent = hand_name_opponent.split("__")[0]

        move_towards = (0, 0)
        if hand_side_opponent == "left" and hand_side_own == "left":
            move_towards = (-0.3, 0.12)
        elif hand_side_opponent == "left" and hand_side_own == "right":
            move_towards = (0.3, 0.12)
        elif hand_side_opponent == "right" and hand_side_own == "right":
            move_towards = (0.3, 0.12)
        elif hand_side_opponent == "right" and hand_side_own == "left":
            move_towards = (-0.3, 0.12)

        return hand_name_own, hand_name_opponent, move_towards

    def get_hand_pos(self, hand_name):
        # player = hand_name.split("__")[1]
        if hand_name is None:
            pos = None
        else:
            hand_idx = self.stim_imgs_hashmap[hand_name]
            hand_img = self.objs.stims[hand_idx].img
            pos = hand_img.pos

        return pos

    def move_hand(self, hand_name, move_towards=None, feint_type=0, reset_pos=False):
        # Transition (movement) of the hand towards the other side
        animation_ends = False
        player = hand_name.split("__")[1]
        hand_idx = self.stim_imgs_hashmap[hand_name]
        hand_img = self.objs.stims[hand_idx].img

        if player == "own":
            t = self.own_hands_speed
            if self.dev_type == 0:
                t = self.own_hands_counterattack_speed
        else:
            t = self.opponent_hands_speed_parallel

        if move_towards is None:
            if player == "own":
                move_towards = (0, -1)
                end_pos_x = move_towards[0]
                end_pos_y = -0.6
                t = self.own_hands_speed
        else:
            end_pos_x = move_towards[0]
            end_pos_y = move_towards[1]
            if feint_type == -1:
                end_pos_x = move_towards[0]
                end_pos_y = move_towards[1]
                if move_towards[0] != 0:
                    t = self.opponent_hands_speed_diagonal
            else:
                if player != "own":
                    t = self.feints_speed_parallel
                    if move_towards[0] != 0:
                        t = self.feints_speed_diagonal
                    end_pos_x = move_towards[0]
                    end_pos_y = self.feints_distances_y[feint_type]

        if reset_pos:
            move_towards = (0, 1)
            end_pos_x = self.objs.stims_def_pos[hand_idx][0]
            end_pos_y = self.objs.stims_def_pos[hand_idx][1]
            # print("POS: ", hand_img.pos[1])
            # print("END POS: ", end_pos_y)

        animation_ends = self.animation_move_hand(end_pos_x, end_pos_y, hand_img, move_towards, t)

        return hand_img, animation_ends

    def animation_move_hand(self, end_pos_x, end_pos_y, hand_img, move_towards, t):
        animation_ends = False

        # print("X: ", hand_img.pos[0], end_pos_x)
        # print("Y: ", hand_img.pos[1], end_pos_y)
        if (move_towards[1] < 0 and hand_img.pos[1] <= end_pos_y) or \
                (move_towards[1] > 0 and hand_img.pos[1] >= end_pos_y) or \
                (hand_img.pos[0] <= -0.35 or hand_img.pos[0] >= 0.35):
            animation_ends = True
        else:
            # print("Moving hand...")
            # Increase the height (y-length in both directions) and then move up on Y so that the base stays put.
            new_x = hand_img.pos[0] + t * move_towards[0]
            new_y = hand_img.pos[1] + t * move_towards[1]
            hand_img.pos = (new_x, new_y)

            # Show it.
            # progTest.draw()
            self.psy.win.flip()

        return animation_ends


    def wait_iti(self, num_trial, show_countdown_text):
        duration_iti = self.duration_iti_before_trial

        # if self.dev_type != -1:
        #     duration_iti = self.duration_iti_before_trial_dev
        if self.session_number == 3:
            duration_iti += np.random.uniform(0, self.duration_max_jitter)

        onset = self.clock_onsets.getTime()

        self.psy.win.flip()
        if show_countdown_text:
            self.show_countdown(duration_iti)
        else:
            psychopy.core.wait(duration_iti)
        psychopy.event.clearEvents()

        return onset

    def calculate_conds_indices(self, num_trials_per_block, per_over_stim=0.5, times_over_stim=10.0, dev_type=-1):
        if num_trials_per_block < self.objs.total_stims:
            raise Exception("Error: num_trials_per_block ({}) must be greater than total number of stims ({})".format(
                num_trials_per_block, self.objs.total_stims))

        num_overtraining_stim, num_undertraining_stim = self.overtraining_stims_stats(per_over_stim)

        times_appear_overtraining_stim = times_over_stim * num_overtraining_stim  # Overtraining stim appear X times more than undertraining stim
        times_appear_undertraining_stim = 1 * num_undertraining_stim
        total_times_appear_stim = times_appear_overtraining_stim + times_appear_undertraining_stim

        mult_overtraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_overtraining_stim_per_block = mult_overtraining_num_stim * times_appear_overtraining_stim
        num_overtraining_stim_per_block = math.floor(num_overtraining_stim_per_block / num_overtraining_stim)
        mult_undertraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_undertraining_stim_per_block = mult_undertraining_num_stim * times_appear_undertraining_stim
        num_undertraining_stim_per_block = math.ceil(num_undertraining_stim_per_block / num_undertraining_stim)

        conds_indices = []

        config_stims_per_block = [num_overtraining_stim_per_block] * num_overtraining_stim + \
                                 [num_undertraining_stim_per_block] * num_undertraining_stim

        for i, num_stim_per_block in enumerate(config_stims_per_block):
            stim_indices = [i] * num_stim_per_block
            conds_indices += stim_indices
        random.shuffle(conds_indices)

        # Include feints:
        num_feints_type_1 = round(self.num_trials_per_block * self.curr_feint_type_1_percentage)
        num_feints_type_2 = round(self.num_trials_per_block * self.feint_type_2_percentage)
        num_no_feints = self.num_trials_per_block - (num_feints_type_1 + num_feints_type_2)
        no_feints_indices = [-1] * num_no_feints
        feints_type_1_indices = [0] * num_feints_type_1
        feints_type_2_indices = [1] * num_feints_type_2

        feints_indices = no_feints_indices + feints_type_1_indices + feints_type_2_indices
        random.shuffle(feints_indices)
        # Check for no more than X consecutive repetitions of the same stimulus
        if num_feints_type_1 > 0 and num_feints_type_2 > 0:
            feints_indices = self.shuffle_stims_with_no_max_consecutives(feints_indices)
        conds_indices = self.shuffle_stims_with_no_max_consecutives(conds_indices)

        if self.dev_type == -1:  # No devaluation
            self.num_trials_per_block = len(conds_indices)
        else:
            self.num_trials_per_block_dev = len(conds_indices)

        return conds_indices, feints_indices

    def overtraining_stims_stats(self, per_over_stim):
        percentage_overtraining_stim = per_over_stim  # Overtraining stim are the X % of the total stim (e.g. if X=50% and there are 4 stimuli, 2 of them will be overtrained stim)
        num_overtraining_stim = int(self.objs.total_stims * percentage_overtraining_stim)
        num_undertraining_stim = self.objs.total_stims - num_overtraining_stim

        return num_overtraining_stim, num_undertraining_stim

    def shuffle_stims_with_no_max_consecutives(self, inds):
        are_max_consecutives = True
        i_consecutive = 1

        while are_max_consecutives:
            ind = 1
            while ind < len(inds):
                # print("Checking for max_consecutives in ind {}".format(ind))
                if inds[ind] == inds[ind-1]:
                    i_consecutive += 1
                else:
                    i_consecutive = 1

                if i_consecutive > self.max_consecutive_stims:  # Reallocate this 3rd consecutive stimulus to not be consecutive
                    # print("Trying to reallocate ind {}".format(ind))
                    insertion_possible = False
                    val_reallocate = inds[ind]
                    while not insertion_possible:
                        rand_ind = np.random.randint(0, len(inds))
                        insertion_possible = self.check_if_max_stim_consecutive_around(val_reallocate, rand_ind, inds)
                    val_substitute = inds[rand_ind]
                    inds[rand_ind] = val_reallocate
                    inds[ind] = val_substitute

                    # Reset search
                    ind = 1
                    i_consecutive = 1
                    # print("Resetting search for new_max because of new reallocations")
                else:
                    ind += 1

            are_max_consecutives = False

        return inds

    def check_if_max_stim_consecutive_around(self, val, ind, inds):
        insertion_possible = False

        start_ind = ind - self.max_consecutive_stims + 1 if ind >= self.max_consecutive_stims else 1
        end_ind = start_ind + (self.max_consecutive_stims * 2)
        if end_ind > len(inds):
            end_ind = len(inds)
        i_consecutive = 1
        for i in range(start_ind, end_ind):
            if inds[i] == val:
                i_consecutive += 1
            else:
                i_consecutive = 1

        if i_consecutive < self.max_consecutive_stims:
            insertion_possible = True

        return insertion_possible

    def calculate_blocks_config_indices(self):
        blocks_config_indices = []

        count_dev = 0
        for i, type in enumerate(self.blocks_design):
            dev_id = -1  # Training
            if type == "dev":
                dev_id = 0
                count_dev += 1
            elif type == "contingency_degradation":
                dev_id = 1
                count_dev += 1
            blocks_config_indices.append(dev_id)

        # dev_block_inds_no_consec = []  # No definitve list, this not includes consecutive dev blocks
        # if self.dev_block_every > 0:
        #     dev_block_inds_no_consec = list(np.arange(self.dev_block_every, self.num_blocks, step=self.dev_block_every))
        #
        # dev_block_inds_no_consec = dev_block_inds_no_consec.copy()
        # dev_block_inds = []
        # for dev_block_idx in dev_block_inds_no_consec:
        #     dev_block_inds.append(dev_block_idx)
        #     for j in range(self.num_consecutive_dev_blocks):
        #         dev_block_inds.append(dev_block_idx+j+1)
        #
        # i_dev = 0
        # for i in range(self.num_blocks):
        #     outcome_dev_id = -1
        #     if i in dev_block_inds:
        #         if i_dev >= len(o_dev_indices):
        #             i_dev = 0
        #         outcome_dev_id = o_dev_indices[i_dev]
        #         i_dev += 1
        #     blocks_config_indices.append(outcome_dev_id)

        return blocks_config_indices

    def draw_outcome_chosen(self, correct, points, outcome_code, delay):
        duration_between_response_outcome = delay

        if not correct:
            # print("----- Playing sound -----")
            # now = ptb.GetSecs()
            if outcome_code == "too_early" or outcome_code == "too_early_both":
                self.objs.sound_too_early.play()
                pass
            else:
                self.objs.sound_slam.play()
                pass
        else:
            if outcome_code == "counterattack_ok":
                self.objs.sound_slam.play()
                time.sleep(0.15)
            self.objs.sound_correct.play()

        if self.session_number == 3:
            duration_between_response_outcome += np.random.uniform(0, self.duration_max_jitter)

        self.black_screen_delay(duration_between_response_outcome)
        outcome_idx = self.outcomes_accuracy_imgs_hashmap[correct]
        self.objs.outcomes[outcome_idx].img.draw()
        self.show_text_points_info(correct, points, outcome_code)

        return 0

    def show_text_points_info(self, correct, points, outcome_code):
        # print("CORRECT: ", correct)
        if correct:
            points = round(points, 2)
            self.objs.text_points_obj_optimal.text = self.outcome_codes_hashmap[outcome_code] + \
                                                     "\n ({} puntos)".format(points)
            self.objs.text_points_obj_optimal.draw()
        else:
            self.objs.text_points_obj_suboptimal.text = self.outcome_codes_hashmap[outcome_code]
            self.objs.text_points_obj_suboptimal.draw()

    def update_results(self, results_dict, onsets):
        # outcome_dev_selected = int(self.objs.outcomes[outcome_idx].is_dev)
        # is_overtraining_stim = self.check_is_overtraining_stim(stim_idx)

        self.results["opponent_hand_idx"].append(results_dict["opponent_hand_idx"])
        self.results["opponent_hand_direction"].append(results_dict["opponent_hand_direction"])
        self.results["own_hand_moving_idx"].append(results_dict["own_hand_moving_idx"])
        self.results["own_hand_target_idx"].append(results_dict["own_hand_target_idx"])
        self.results["own_hand_moving_other_idx"].append(results_dict["own_hand_moving_other_idx"])
        self.results["dodge_ok"].append(results_dict["dodge_ok"])
        self.results["feint_type"].append(results_dict["feint_type"])
        self.results["counterattack_ok"].append(results_dict["counterattack_ok"])
        self.results["response_time"].append(results_dict["response_time"])
        self.results["opponent_hand_x"].append(results_dict["opponent_hand_x"])
        self.results["opponent_hand_y"].append(results_dict["opponent_hand_y"])
        self.results["points_this_trial"].append(results_dict["points_this_trial"])
        self.results["outcome_code"].append(results_dict["outcome_code"])

        self.results["onset_fixation"].append(onsets["fixation"])
        self.results["onset_attack_starts"].append(onsets["attack_starts"])
        self.results["onset_attack_ends"].append(onsets["attack_ends"])
        self.results["onset_dodge_starts"].append(onsets["dodge_starts"])
        self.results["onset_dodge_ends"].append(onsets["dodge_ends"])
        self.results["onset_counterattack_starts"].append(onsets["counterattack_starts"])
        self.results["onset_counterattack_ends"].append(onsets["counterattack_ends"])
        self.results["onset_stimulus"].append(onsets["stimulus"])
        self.results["onset_outcome_presentation"].append(onsets["outcome_presentation"])
        self.results["onset_iti"].append(onsets["iti"])
        self.results["onset_end_trial"].append(onsets["end_trial"])

        self.set_total_points()

    def set_total_points(self):
        total_points = self.results["total_points"][-1] if len(self.results["total_points"]) > 0 else 0
        self.results["total_points"].append(total_points + self.results["points_this_trial"][-1])

    def check_correct(self, hand_name_own, hand_name_own_moving, hand_name_own_moving_other, hand_name_opponent, feint_type=-1):
        correct = False
        outcome_code = "too_late"

        if hand_name_own_moving is None and feint_type != -1:
            outcome_code = "no_error_feint"
            if hand_name_own_moving_other:
                outcome_code = "no_error_feint_both"
            correct = True
            return correct, outcome_code

        if hand_name_opponent is None:
            outcome_code = "too_early"
            if hand_name_own_moving_other:
                outcome_code = "too_early_both"
        elif hand_name_own_moving is None:
            outcome_code = "too_late"
        elif hand_name_own != hand_name_own_moving:
            if self.dev_type == 0:
                hand_own_idx = self.stim_imgs_hashmap[hand_name_own_moving]
                hand_own_img = self.objs.stims[hand_own_idx].img
                hand_opponent_idx = self.stim_imgs_hashmap[hand_name_opponent]
                hand_opponent_img = self.objs.stims[hand_opponent_idx].img

                hand_opponent_y = hand_opponent_img.pos[1] - hand_opponent_img.size[1] / 2
                if hand_opponent_y < self.midline_y:
                    collision = hand_own_img.overlaps(hand_opponent_img)
                    correct = collision  # Dodge ok
                    if correct:
                        outcome_code = "counterattack_ok"
                    else:
                        outcome_code = "counterattack_error"
                else:
                    outcome_code = "too_early"
            else:
                outcome_code = "opposite_hand"
                if hand_name_own_moving_other:
                    outcome_code = "opposite_hand_both"
        elif hand_name_own_moving is not None and hand_name_opponent is not None:
            hand_own_idx = self.stim_imgs_hashmap[hand_name_own_moving]
            hand_own_img = self.objs.stims[hand_own_idx].img
            hand_opponent_idx = self.stim_imgs_hashmap[hand_name_opponent]
            hand_opponent_img = self.objs.stims[hand_opponent_idx].img

            hand_opponent_y = hand_opponent_img.pos[1] - hand_opponent_img.size[1] / 2
            if hand_opponent_y < self.midline_y:
                collision = hand_own_img.overlaps(hand_opponent_img)
                correct = not collision  # Dodge ok
                if correct:
                    outcome_code = "no_error"
                else:
                    outcome_code = "too_late"
                    if hand_name_own_moving_other:
                        outcome_code = "too_late_both"
            else:
                outcome_code = "too_early"

        return correct, outcome_code

    def accuracy_consumption_trial(self, outcome_inds, points_earned):
        accuracy = 0
        max_possible_points = 0
        outcome_not_selected_id = -1

        for i, outcome_idx in enumerate(outcome_inds):
            possible_outcome = self.objs.outcomes[outcome_idx].points
            if possible_outcome > max_possible_points:
                max_possible_points = possible_outcome
            if points_earned != possible_outcome:
                outcome_not_selected_id = outcome_idx

        if points_earned < max_possible_points:
            accuracy = 0  # Suboptimal response
        else:
            accuracy = 1  # Optimal response

        return accuracy, outcome_not_selected_id, max_possible_points

    def check_is_overtraining_stim(self, stim_id):
        is_overtraining_stim = 0
        if stim_id in self.stim_overtraining_indices:
            is_overtraining_stim = 1

        return is_overtraining_stim

    def counterbalancing_o_dev(self):
        o_dev_indices = self.stim_overtraining_indices
        o_dev_indices_combs = list(itertools.permutations(o_dev_indices, len(o_dev_indices)))
        participant_number = self.participant_number
        if participant_number > len(o_dev_indices) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(o_dev_indices))) * len(o_dev_indices)

        o_dev_indices = list(o_dev_indices_combs[participant_number])

        return o_dev_indices

    def black_screen_delay(self, time):
        self.psy.win.flip(clearBuffer=True)
        psychopy.core.wait(time)

        return 0
