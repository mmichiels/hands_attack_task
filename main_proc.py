import math

from psychopy import prefs
prefs.hardware['audioLib'] = ['PTB']
from psychopy import sound
import utils_psychopy as utils_psy
import instructions as instr
import Experiment as exp
import multiprocessing

debug = False
instructions = False
in_fMRI = False
session_number = 2

if __name__ == "__main__":
    psy_obj = utils_psy.Psychopy(debug, session_number, in_fMRI)

    experiment = exp.Experiment(psy_obj)
    if instructions:
        instr.show_instructions(psy_obj, experiment.instruction_keys)

    experiment.run()

    psy_obj.save_results(experiment.results, format="csv")